package com.example.minios3v2client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.minios3v2lib.MinioS3;

import java.io.IOException;

public class ListBucketActivity extends AppCompatActivity {

    public static final String BUCKET_NAME = "bucket_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_bucket);

        RecyclerView recyclerView =  findViewById(R.id.recyclerViewBucket);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        Intent intent = getIntent();
        String tipe = intent.getStringExtra(MainActivity.OPERATION);
        BucketListAdapter mAdapter = null;
        try {
            mAdapter = new BucketListAdapter(MinioS3.GetStringListBucket(),this,tipe);
        } catch (IOException e) {
            e.printStackTrace();
        }

        recyclerView.setAdapter(mAdapter);
    }
}
