package com.example.minios3v2client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.minios3v2lib.MinioS3;

import java.io.IOException;

public class ListObjectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_object);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewObject);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        Intent intent = getIntent();
        String bucketName = intent.getStringExtra(ListBucketActivity.BUCKET_NAME);
        String objectOps = intent.getStringExtra(OperationActivity.OBJECT);

        ObjectListAdapter mAdapter = null;
        try {
            mAdapter = new ObjectListAdapter(MinioS3.GetStringListObject(bucketName), this, bucketName, objectOps);
        } catch (IOException e) {
            e.printStackTrace();
        }
        recyclerView.setAdapter(mAdapter);
    }
}
