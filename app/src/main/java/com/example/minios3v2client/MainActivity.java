package com.example.minios3v2client;

import android.Manifest;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.minios3v2lib.MinioS3;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public static final String OPERATION = "operation";
    public static final String OPERATION_DELETE = "delete";
    public static final String OPERATION_LIST = "list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            MinioS3.Init("Q3AM3UQ867SPQQA43P2F",
                    "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
                    "https://play.min.io:9000");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Button btnBucket = findViewById(R.id.btnBucket);
        btnBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),BucketActivity.class);
                startActivity(intent);
            }
        });

        Button btnListBucket = findViewById(R.id.btnListBucket);
        btnListBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ListBucketActivity.class);
                intent.putExtra(OPERATION,OPERATION_LIST);
                startActivity(intent);
            }
        });

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                99);
    }
}
