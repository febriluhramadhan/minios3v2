package com.example.minios3v2client;

import android.app.Activity;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minios3v2lib.MinioS3;

import java.io.IOException;
import java.util.List;

public class ObjectListAdapter extends RecyclerView.Adapter<ObjectListAdapter.MyViewHolder>{

    List<String> objectData;
    Activity activity;
    String bucketName;
    boolean isDelete = false;

    public ObjectListAdapter(List<String> data, Activity activity,String bucketName,String tipe)
    {
        this.objectData = data;
        this.activity = activity;
        this.bucketName = bucketName;
        this.isDelete = tipe.equals(OperationActivity.OBJECT_DELETE);
    }

    @NonNull
    @Override
    public ObjectListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_object, viewGroup, false);
        ObjectListAdapter.MyViewHolder vh = new ObjectListAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ObjectListAdapter.MyViewHolder myViewHolder, int i) {
        final String objectName = objectData.get(i);
        myViewHolder.textView.setText(objectName);
        myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isDelete) {
                    try {
                        String downloadPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath() + "/" + objectName;
                        MinioS3.DownloadObject(bucketName, objectName, downloadPath);
                        Toast.makeText(view.getContext(), "Download Success \n" + downloadPath, Toast.LENGTH_LONG).show();
                        activity.finish();
                    } catch (IOException e) {
                        Toast.makeText(view.getContext(), "Download " + objectName + " Failed \n" + e.getStackTrace(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    try {
                        MinioS3.DeleteObject(bucketName, objectName);
                        Toast.makeText(view.getContext(), "Delete " + objectName + "Success", Toast.LENGTH_LONG).show();
                        activity.finish();
                    } catch (IOException e) {
                        Toast.makeText(view.getContext(), "Delete " + objectName + " Failed \n" + e.getStackTrace(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return objectData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;
        public LinearLayout linearLayout;
        public MyViewHolder(View v) {
            super(v);
            this.textView = itemView.findViewById(R.id.textObjectName);
            linearLayout = itemView.findViewById(R.id.layoutObject);
        }
    }
}
