package com.example.minios3v2client;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.minios3v2lib.MinioS3;

import java.io.IOException;

public class BucketActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bucket);

        EditText editTextBucket = findViewById(R.id.editTextBucket);

        Button btnCreateBucket = findViewById(R.id.btnCreateBucket);
        btnCreateBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bucketName = editTextBucket.getText().toString();
                editTextBucket.onEditorAction(EditorInfo.IME_ACTION_DONE);

                if(bucketName.isEmpty())
                {
                    Toast.makeText(getApplicationContext(),"Error : Fill input field", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    MinioS3.CreateBucket(bucketName);
                    Toast.makeText(getApplicationContext(),"Create Bucket "+bucketName+" Success", Toast.LENGTH_LONG).show();
                }catch (IOException e)
                {
                    Toast.makeText(getApplicationContext(),"Create Bucket "+bucketName+" Failed \n"+e.getStackTrace(), Toast.LENGTH_LONG).show();
                }
            }
        });

        Button btnDeleteBucket = findViewById(R.id.btnDeleteBucket);
        btnDeleteBucket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ListBucketActivity.class);
                intent.putExtra(MainActivity.OPERATION,MainActivity.OPERATION_DELETE);
                startActivity(intent);
            }
        });
    }
}
