package com.example.minios3v2client;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minios3v2lib.MinioS3;

import java.io.IOException;
import java.util.List;

public class BucketListAdapter extends RecyclerView.Adapter<BucketListAdapter.MyViewHolder> {

    List<String> bucketData;
    Activity activity;
    Boolean isDelete = false;

    public BucketListAdapter(List<String> data, Activity activity, String tipe)
    {
        this.bucketData = data;
        this.activity = activity;
        this.isDelete = tipe.equals(MainActivity.OPERATION_DELETE);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_bucket, viewGroup, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        final String bucketName = bucketData.get(i);
        myViewHolder.textView.setText(bucketName);
        myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isDelete) {
                    Intent intent = new Intent(activity,OperationActivity.class);
                    intent.putExtra(ListBucketActivity.BUCKET_NAME,bucketName);
                    activity.startActivity(intent);
                }
                else
                {
                    try {
                        MinioS3.DeleteBucketAndAllContents(bucketName);
                        Toast.makeText(view.getContext(), "Delete Bucket " + bucketName +" Success", Toast.LENGTH_LONG).show();
                        activity.finish();
                    } catch (IOException e) {
                        Toast.makeText(view.getContext(), "Delete Bucket " + bucketName +" Failed \n"+e.getStackTrace(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bucketData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textView;
        public LinearLayout linearLayout;
        public MyViewHolder(View v) {
            super(v);
            this.textView = itemView.findViewById(R.id.textBucketName);
            linearLayout = itemView.findViewById(R.id.layoutBucket);
        }
    }

}
