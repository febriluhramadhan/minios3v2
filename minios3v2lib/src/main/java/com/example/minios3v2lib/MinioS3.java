package com.example.minios3v2lib;

import android.os.StrictMode;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.Bucket;
import software.amazon.awssdk.services.s3.model.CreateBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteBucketRequest;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.ListBucketsRequest;
import software.amazon.awssdk.services.s3.model.ListBucketsResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsResponse;
import software.amazon.awssdk.services.s3.model.NoSuchBucketException;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.S3Object;
import software.amazon.awssdk.testutils.Waiter;

public class MinioS3 {
    static S3Client s3Client;
    public static  void Init(String accessKey,String secretKey,String endPoint)throws IOException
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        AwsBasicCredentials awsCreds = AwsBasicCredentials.create(accessKey,secretKey);

        s3Client = S3Client.builder()
                .credentialsProvider(StaticCredentialsProvider.create(awsCreds))
                .endpointOverride(URI.create(endPoint))
                .region(Region.US_EAST_1)
                .httpClientBuilder(UrlConnectionHttpClient.builder())
                .build();
    }

    public static void CreateBucket(String bucketName) throws IOException
    {
        if(!IsBucketExist(bucketName))
        {
            CreateBucketRequest createBucketRequest = CreateBucketRequest
                    .builder()
                    .bucket(bucketName.toLowerCase())
                    .build();
            s3Client.createBucket(createBucketRequest);
        }
        else
        {
            Log.e("CreateBucket",bucketName+" is Exist");
        }
    }

    public static boolean IsBucketExist(String bucketName) throws IOException
    {
        boolean isExist = false;
        for (Bucket bucket: GetListBucket())
        {
            if(bucket.name().equals(bucketName.toLowerCase()))
            {
                isExist = true;
            }
        }
        return isExist;
    }

    public static List<Bucket> GetListBucket() throws IOException
    {
        ListBucketsRequest listBucketsRequest = ListBucketsRequest.builder().build();
        ListBucketsResponse listBucketsResponse = s3Client.listBuckets(listBucketsRequest);
        return listBucketsResponse.buckets();
    }

    public static List<String> GetStringListBucket() throws IOException
    {
        List<String> stringBucket = new ArrayList<>();
        for (Bucket bucket: GetListBucket())
        {
            stringBucket.add(bucket.name());
        }
        return stringBucket;
    }

    public static List<S3Object> GetListObject(String bucketName) throws IOException
    {
        ListObjectsRequest listBucketsRequest = ListObjectsRequest.builder().bucket(bucketName).build();
        ListObjectsResponse listBucketsResponse = s3Client.listObjects(listBucketsRequest);
        return listBucketsResponse.contents();
    }

    public static List<String> GetStringListObject(String bucketName) throws IOException
    {
        List<String> stringObject = new ArrayList<>();
        for (S3Object object: GetListObject(bucketName))
        {
            stringObject.add(object.key());
        }
        return stringObject;
    }

    public static void DownloadObject(String bucketName,String keyName,String pathFile) throws IOException
    {
        File file = new File(pathFile);
        if(!file.exists()) {
            s3Client.getObject(GetObjectRequest.builder().bucket(bucketName).key(keyName).build(),
                    ResponseTransformer.toFile(file));
        }else
        {
            Log.e("Download Object : ",keyName+" is Exist");
        }
    }

    public static void DeleteObject(String bucketName,String keyName) throws IOException
    {
        DeleteObjectRequest deleteObjectRequest = DeleteObjectRequest.builder().bucket(bucketName).key(keyName).build();
        s3Client.deleteObject(deleteObjectRequest);
    }

    public static void DeleteBucketAndAllContents(String bucketName)  throws IOException
    {
        try {
            System.out.println("Deleting S3 bucket: " + bucketName);
            ListObjectsResponse response = Waiter.run(() -> s3Client.listObjects(r -> r.bucket(bucketName)))
                    .ignoringException(NoSuchBucketException.class)
                    .orFail();
            List<S3Object> objectListing = response.contents();

            if (objectListing != null) {
                while (true) {
                    for (Iterator<?> iterator = objectListing.iterator(); iterator.hasNext(); ) {
                        S3Object objectSummary = (S3Object) iterator.next();
                        s3Client.deleteObject(DeleteObjectRequest.builder().bucket(bucketName).key(objectSummary.key()).build());
                    }

                    if (response.isTruncated()) {
                        objectListing = s3Client.listObjects(ListObjectsRequest.builder()
                                .bucket(bucketName)
                                .marker(response.marker())
                                .build())
                                .contents();
                    } else {
                        break;
                    }
                }
            }

            s3Client.deleteBucket(DeleteBucketRequest.builder().bucket(bucketName).build());
        } catch (Exception e) {
            System.err.println("Failed to delete bucket: " + bucketName);
            e.printStackTrace();
        }
    }

    public static  void Upload(String bucketName, String keyName, File file) throws IOException
    {
        PutObjectRequest putObjectRequest = PutObjectRequest.builder()
                .bucket(bucketName)
                .key(keyName)
                .build();

        s3Client.putObject(putObjectRequest, RequestBody.fromFile(file));
    }
}
